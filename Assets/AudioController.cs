﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private void Awake()
    {
        if (Instanz != null)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(this);
        Instanz = this;
    }

    public static AudioController Instanz;

    public AudioSource _srcMusic;
    public AudioSource _srcCastEffect;
    public AudioSource[] _srcEffects;

    public void SetCastEffectStatus(bool status)
    {
        if (status && _srcCastEffect.isPlaying == false)
            _srcCastEffect.Play();
        else if (status == false && _srcCastEffect.isPlaying)
            _srcCastEffect.Pause();
    }

    public void SetMusicVolume(float volume)
    {
        _srcMusic.volume = volume * 0.5f;
    }

    public void SetEffectVolume(float volume)
    {
        _srcCastEffect.volume = volume;
        foreach (var srcEffect in _srcEffects)
            srcEffect.volume = volume;
    }

    public void PlaySound(AudioClip clip)
    {
        var source = _srcEffects.FirstOrDefault(p => p.isPlaying == false);

        if (source == null)
            return;

        source.clip = clip;
        source.Play();
    }
}
