﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelper : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Instanz = this;
    }

    public static AudioHelper Instanz;

    public AudioClip plantShakeEffect;
    public AudioClip plantSpawnEffect;
    public AudioClip levelUpEffect;
}
