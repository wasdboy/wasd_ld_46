﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ButterflyController : MonoBehaviour, IAnimal
{
    void Start()
    {
        if (_randomFarbe == false)
            return;

        _randomColor = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
        _renderer.color = _randomColor;
    }

    private Color _randomColor;
    [SerializeField]
    private SpriteRenderer _renderer;

    [SerializeField]
    private Animator _animator;

    private Entity _ziel;

    private bool _idle;

    [SerializeField]
    private bool _randomFarbe;

    private float _idleZeit;

    private void Update()
    {
        if (_idle)
            Idle();
        else
            BlumeSuchen();
    }

    private void BlumeSuchen()
    {
        if (_ziel == null)
            _ziel = EntityController.Instanz.GetRandomFlower();

        if (_ziel == null)
            return;

        transform.position = Vector2.MoveTowards(transform.position, _ziel.transform.position, 1f * Time.deltaTime);
        var abstand = Mathf.Abs(Vector2.Distance(transform.position, _ziel.transform.position));
        if (abstand < 0.1f)
        {
            _idle = true;
            _idleZeit = Random.Range(3f, 15f);            
            _animator.SetTrigger("Idle");
            _ziel.Animals.Add(this);
        }
    }

    private void Idle()
    {
        if (_idleZeit > 0)
        {
            _idleZeit -= Time.deltaTime;
            return;
        }

        _ziel.Animals.Remove(this);
        _ziel = null;
        _idle = false;
        _animator.SetTrigger("Fly");
    }

    public void NeueZielSuchen()
    {
        _ziel.Animals.Remove(this);
        _ziel = null;
        _idle = false;
        _animator.SetTrigger("Fly");
    }
}
