﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProgressPanel : MonoBehaviour
{
    private void Awake()
    {
        Instanz = this;
    }

    public static ProgressPanel Instanz;
    public GameObject EndePanel;

    public TextMeshProUGUI _txtAnzahl;

    private int _count;

    public void PflanzeHinzufuegen()
    {
        _count++;
        _txtAnzahl.text = _count.ToString();

        if (_count == 500)
            EndePanel.SetActive(true);
    }
}
