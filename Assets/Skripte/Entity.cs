﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public void Anmelden()
    {
        Animals = new List<IAnimal>();
        EntityController.Instanz.Anmelden(Type, gameObject);
    }

    public EnumEntityType Type;
    public List<IAnimal> Animals;

    public void Gewackelt()
    {
        for (int i = Animals.Count -1; i >= 0; i--)
        {
            var animal = Animals[i];
            animal.NeueZielSuchen();
        }
    }
}
