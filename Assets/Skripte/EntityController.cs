﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumEntityType { Flower, Tree, Mushrooms}

public class EntityController : MonoBehaviour
{
    void Start()
    {
        Instanz = this;

        Flowers = new List<GameObject>();
        Mushrooms = new List<GameObject>();
        Trees = new List<GameObject>();
    }

    public static EntityController Instanz;

    public List<GameObject> Flowers;
    public List<GameObject> Mushrooms;
    public List<GameObject> Trees;

    public void Anmelden(EnumEntityType type, GameObject go)
    {
        switch (type)
        {
            case EnumEntityType.Flower:
                Flowers.Add(go);
                break;
            case EnumEntityType.Tree:
                Trees.Add(go);                
                break;
            case EnumEntityType.Mushrooms:
                Mushrooms.Add(go);
                break;
            default:
                Debug.Log("BISCHE DOOF?!?");
                break;
        }
    }

    public Entity GetRandomFlower()
    {
        if (Flowers.Count <= 0)
            return null;

        return Flowers[Random.Range(0, Flowers.Count)].GetComponent<Entity>();
    }

    public Entity GetRandomTree()
    {
        if (Trees.Count <= 0)
            return null;

        return Trees[Random.Range(0, Trees.Count)].GetComponent<Entity>();
    }
}
