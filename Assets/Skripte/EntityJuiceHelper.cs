﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityJuiceHelper : MonoBehaviour
{
    void Start()
    {
        _animator = GetComponent<Animator>();

        if(_particleSystem == null)
            _particleSystem = GetComponent<ParticleSystem>();

        _entity = GetComponentInChildren<Entity>();
    }

    private Animator _animator;

    [SerializeField]
    private ParticleSystem _particleSystem;

    [SerializeField]
    private int _anzahlPartikel = 0;    

    public Transform TransformGrafik;

    private Entity _entity;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_animator != null && TransformGrafik.localScale == Vector3.one)
        {
            _animator.SetTrigger("Shake");
            AudioController.Instanz.PlaySound(AudioHelper.Instanz.plantShakeEffect);

            if (_entity != null)
                _entity.Gewackelt();
        }
    }

    public void PartikelErzeugen()
    {
        _particleSystem.Emit(_anzahlPartikel);
    }
}
