﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpPanel : MonoBehaviour
{
    private void Start()
    {
        LevelUpKontroller.Instanz.ExpChanged += OnExpChanged;
    }

    [SerializeField]
    private Image _image = null;

    public void OnExpChanged(int aktuell, int max)
    {
        _image.fillAmount = (float)aktuell / (float)max ;
    }
}
