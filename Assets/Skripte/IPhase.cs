﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface IPhase
{
    Vector3 Positon { get; set; }
    float Prozent { get; set; }

    void Wachse();
    void Verrotte();

    event Action<IPhase> OnPhaseWachsenEndeHandler;
}

