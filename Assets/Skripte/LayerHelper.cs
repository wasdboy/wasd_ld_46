﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerHelper : MonoBehaviour
{
    [SerializeField]
    private bool _statisch = false;

    [SerializeField]
    private SpriteRenderer _renderer;

    void Start()
    {
        if(_renderer == null)
            _renderer = GetComponent<SpriteRenderer>();
        _renderer.sortingOrder = (int)(((float)transform.position.y) * -100f) ;
    }

    private void Update()
    {
        if (_statisch)
            return;
        _renderer.sortingOrder = (int)(((float)transform.position.y) * -100f);
    }
}
