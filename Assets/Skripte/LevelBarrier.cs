﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBarrier : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LevelUpKontroller.Instanz.LevelUp += OnLevelUp;

        _children = new List<GameObject>();
        foreach (Transform child in transform)
        {
            _children.Add(child.gameObject);
            child.gameObject.SetActive(false);
        }

        OnLevelUp();
    }

    [SerializeField]
    private int _erfoderlichesLevel;

    private List<GameObject> _children;

    public void OnLevelUp()
    {
        if(LevelUpKontroller.Instanz.Level >= _erfoderlichesLevel)
            foreach (Transform child in transform)
                child.gameObject.SetActive(true);
    }
}
