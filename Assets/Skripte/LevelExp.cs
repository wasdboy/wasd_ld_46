﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class LevelExp
{
    static LevelExp()
    {
        _levelExpMap = new int []
        {
            5, 7,10,15,20,25,30,40,50,60,70,80,100,120,140,160,180,200,225,250,275,300,350,400,450,500
        };
    }

    private static int[] _levelExpMap;

    public static int GetBenoetigteExp(int level)
    {
        if (level >= _levelExpMap.Length)
            return _levelExpMap.Last();

        return _levelExpMap[level];
    }
}

