﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelLabel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LevelUpKontroller.Instanz.LevelUp += LevelUp;
        LevelUp();
    }

    [SerializeField]
    private TextMeshProUGUI _levelLabel;

    private void OnEnable()
    {
        LevelUp();
    }

    public void LevelUp()
    {
        _levelLabel.text = string.Format("Level: {0}", LevelUpKontroller.Instanz.Level.ToString());
    }
}
