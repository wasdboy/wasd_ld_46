﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpKontroller : MonoBehaviour
{
    private void Awake()
    {
        Instanz = this;
    }

    public static LevelUpKontroller Instanz;

    private  int _aktuelleExp = 0;
    private  int _aktuellesLevel = 0;

    private  int _aktuelleSkillPunkte = 0;

    public  event Action LevelUp;
    public  event Action SkillPunkteChanged;
    public  event Action<int,int> ExpChanged;

    public int Skillpunkte { get { return _aktuelleSkillPunkte; } }
    public int Level { get { return _aktuellesLevel; } }

    public  SkillUi[] _skills;

    public  void ExpErhalten(int exp)
    {
        _aktuelleExp += exp;
        var benoetigteExp = LevelExp.GetBenoetigteExp(_aktuellesLevel);

        while (_aktuelleExp >= benoetigteExp)
        {
            _aktuelleExp -= benoetigteExp;
            _aktuellesLevel++;
            _aktuelleSkillPunkte++;

            benoetigteExp = LevelExp.GetBenoetigteExp(_aktuellesLevel);

            LevelUp();

            AudioController.Instanz.PlaySound(AudioHelper.Instanz.levelUpEffect);


            if (SkillPunkteChanged!= null)
            SkillPunkteChanged();
        }

        if(ExpChanged!= null)
            ExpChanged(_aktuelleExp, benoetigteExp);
    }

    public  void IncreasePlantSpawnChance(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.PflanzungsPhaseUebergangsWert /= 2;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void IncreaseSpellPower(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.ZauberKraftProzent += 0.0075f;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void IncreaseMoveSpeed(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.Geschwindigkeit += 1f;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void IncreaseZauberRadius(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.ZauberRadius += 0.25f;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void KannBlumenSpawnen(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        ObjektSpawnKontroller.Instanz.KannBlumenSpawnen = true;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void KannBaeumeSpawnen(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        ObjektSpawnKontroller.Instanz.KannBaeumeSpawnen = true;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void KannPilzeSpawnen(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        ObjektSpawnKontroller.Instanz.KannPilzeSpawnen = true;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void KannTiereSpawnen(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.KannTiereSpawnen = true;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public void ErhoeheLaufgeschwindigkeitBeimZaubern(string uiname)
    {
        if (KannSkillLernen == false)
            return;

        SpielerController.ZauberBewegungsMalus += 0.2f;
        _aktuelleSkillPunkte--;
        _skills.FirstOrDefault(p => p.name == uiname).SkillKaufen();
        SkillPunkteChanged();
    }

    public  bool KannSkillLernen { get { return _aktuelleSkillPunkte >= 1; } }
}
