﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpPanel : MonoBehaviour
{
    private void Start()
    {
        LevelUpKontroller.Instanz.LevelUp += OnLevelUp;
    }

    [SerializeField]
    private ParticleSystem _particleSystem;

    [SerializeField]
    private GameObject _button;

    public void OnLevelUp()
    {
        _button.SetActive(true);
        _particleSystem.Emit(20);
    }
}
