﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTileMap : MonoBehaviour
{
    private void Start()
    {
        _tiles = new GameObject[_tileMapGroesse, _tileMapGroesse];

        for (int x = 0; x < _tileMapGroesse; x++)
        {
            for (int y = 0; y < _tileMapGroesse; y++)
            {
                var tile = Instantiate(_tilePrefab, new Vector2(x - _offset, y - _offset), Quaternion.identity);
                _tiles[x, y] = tile;

                tile.GetComponent<TileWachsenEffekt>().WiesenSprite = _wiesenSprites[Random.Range(0, _wiesenSprites.Length)];
                tile.GetComponent<TileWachsenEffekt>().BodenRenderer.sprite = _bodenSprites[Random.Range(0, _bodenSprites.Length)];
            }
        }
        var edgecollider = GetComponent<EdgeCollider2D>();
        edgecollider.points = new[] {
            bottomLeft 
            , new Vector2(bottomLeft.x , bottomLeft.y + _tileMapGroesse)
            , new Vector2(bottomLeft.x +_tileMapGroesse, bottomLeft.y + _tileMapGroesse)
            , new Vector2(bottomLeft.x + _tileMapGroesse, bottomLeft.y)
            , new Vector2(bottomLeft.x, bottomLeft.y )
        };
    }

    [SerializeField]
    private GameObject _tilePrefab = null;
    [SerializeField]
    private int _tileMapGroesse = 0;
    [SerializeField]
    private Sprite[] _wiesenSprites = null;
    [SerializeField]
    private Sprite[] _bodenSprites = null;

    private GameObject[,] _tiles;
    private float _offset { get { return _tileMapGroesse / 2; } }
    private Vector2 bottomLeft { get { return new Vector2((_tileMapGroesse / 2 + 0.5f) * -1, (_tileMapGroesse / 2 + 0.5f) * -1); } }
    
}
