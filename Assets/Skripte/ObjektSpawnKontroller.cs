﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjektSpawnKontroller : MonoBehaviour
{
    private void Awake()
    {
        Instanz = this;
    }

    public static ObjektSpawnKontroller Instanz;

    [SerializeField]
    private List<GameObject> _baeume = null;

    [SerializeField]
    private List<GameObject> _graeser = null;

    [SerializeField]
    private List<GameObject> _blumen = null;

    [SerializeField]
    private List<GameObject> _pilze = null;

    public bool KannBlumenSpawnen;
    public bool KannBaeumeSpawnen;
    public bool KannPilzeSpawnen;

    public GameObject Instanzieren(Vector3 position)
    {
        GameObject prefab;

        if (Random.Range(0, 100) <= 5f && KannBaeumeSpawnen)
            prefab = _baeume[Random.Range(0, _baeume.Count )];
        else
        {
            var random = Random.Range(0, 100);
            if(random<= 10 && KannPilzeSpawnen)
                prefab = _pilze[Random.Range(0, _pilze.Count)];
            else if(random <= 40 && KannBlumenSpawnen)
                prefab = _blumen[Random.Range(0, _blumen.Count)];
            else                
                prefab = _graeser[Random.Range(0, _graeser.Count)];
        }

        return Instantiate(prefab, position, Quaternion.identity);
    }

    public void Zerstoeren(GameObject go)
    {
        Destroy(go);
    }
}
