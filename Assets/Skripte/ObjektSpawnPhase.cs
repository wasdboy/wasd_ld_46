﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ObjektSpawnPhase : IPhase
{
    public ObjektSpawnPhase()
    {
        Prozent = 0f;
    }
        
    public event Action<IPhase> OnPhaseWachsenEndeHandler;

    private IPhase _naechstePhase = null;

    public float Prozent { get; set; }
    private float _verfallInSekunden = 30f;
    private GameObject _objekt;
    private float _verfall { get { return  1f / (20f * _verfallInSekunden); } }
    private EntityJuiceHelper _helper;
    private bool _fertigGewachsen;
    public Vector3 Positon { get; set; }

    public void Verrotte()
    {
        if (_fertigGewachsen || Prozent <= 0)
            return;

        Prozent -= _verfall;

        if(_helper != null)
            _helper.TransformGrafik.localScale = new Vector3(Prozent, Prozent, 1);

        if (Prozent <= 0)
        {
            Prozent = 0;
            ObjektSpawnKontroller.Instanz.Zerstoeren(_objekt);
            _objekt = null;
        }
    }

    public void Wachse()
    {
        if (_fertigGewachsen)
            return;

        if (_objekt == null)
        {
            _objekt = ObjektSpawnKontroller.Instanz.Instanzieren(Positon);
            _helper = _objekt.GetComponent<EntityJuiceHelper>();
        }

        Prozent += SpielerController.ZauberKraftProzent * SpielerController.ObjektSpawnPhaseMalus;
        _helper.TransformGrafik.localScale = new Vector3(Prozent, Prozent, 1);

        if (Prozent > 1f)
            Prozent = 1;

        if (Prozent == 1 && _fertigGewachsen == false)
        {
            LevelUpKontroller.Instanz.ExpErhalten(1);
            _fertigGewachsen = true;
            _helper.PartikelErzeugen();
            _helper.TransformGrafik.localScale = new Vector3(1, 1, 1);
            AudioController.Instanz.PlaySound(AudioHelper.Instanz.plantSpawnEffect);

            ProgressPanel.Instanz.PflanzeHinzufuegen();

            var entity = _objekt.GetComponentInChildren<Entity>();
            if (entity != null)
            {
                entity.Anmelden();
            }
        }

        if (_naechstePhase != null)
            OnPhaseWachsenEndeHandler(_naechstePhase);
    }
}

