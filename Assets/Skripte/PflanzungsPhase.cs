﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PflanzungsPhase : IPhase
{
    public PflanzungsPhase(Sprite wiese, SpriteRenderer wiesenRenderer, SpriteRenderer bodenRenderer)
    {
        WiesenSprite = wiese;
        WiesenRenderer = wiesenRenderer;
        BodenRenderer = bodenRenderer;

        _naechstePhase = new ObjektSpawnPhase();

        _aktuelleTexture = new Texture2D(16, 16);
        _aktuelleTexture.filterMode = FilterMode.Point;

        var sprite = Sprite.Create(_aktuelleTexture, new Rect(0, 0, 16, 16), new Vector2(0.5f, 0.5f), 16);
        WiesenRenderer.sprite = sprite;

        _aktuellePixel = new Color[_aktuelleTexture.GetPixels().Length];

        for (int i = 0; i < _aktuellePixel.Length; i++)
            _aktuellePixel[i] = new Color(0, 0, 0, 0);

        _aktuelleTexture.SetPixels(_aktuellePixel);
        _aktuelleTexture.Apply();

        var offset = WiesenSprite.textureRectOffset;
        var rect = WiesenSprite.textureRect;
        _zielPixel = WiesenSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);

        _gefaerbtePixel = new List<int>();
        _transparentePixel = new List<int>();

        for (int i = 0; i < _aktuellePixel.Length; i++)
            _transparentePixel.Add(i);
    }

    private IPhase _naechstePhase;
    public float Prozent { get; set; }

    private Color[] _aktuellePixel;
    private Color[] _zielPixel;

    private List<int> _transparentePixel;
    private List<int> _gefaerbtePixel;

    private float _verfallProzent = 0.00625f;

    public Sprite WiesenSprite;
    public SpriteRenderer WiesenRenderer;
    public SpriteRenderer BodenRenderer;

    private Texture2D _aktuelleTexture;

    public Vector3 Positon { get; set; }

    public event Action<IPhase> OnPhaseWachsenEndeHandler;

    public void Verrotte()
    {
        if ( Prozent == 0)
            return;

        var zuVerottendePixel = (((float)_zielPixel.Length) * (1f - Prozent));
        var differenz = zuVerottendePixel - _transparentePixel.Count;

        for (int i = 0; i < differenz - 1; i++)
        {
            var positon = GetGefaerbterPixel();
            if (positon <= 0)
                break;

            _aktuellePixel[positon] = new Color(0, 0, 0, 0);

            _transparentePixel.Add(positon);
            _gefaerbtePixel.Remove(positon);
        }

        _aktuelleTexture.SetPixels(_aktuellePixel);
        _aktuelleTexture.Apply();

        Prozent -= _verfallProzent;
        if (Prozent < 0)
        {
            Prozent = 0;
        }
    }

    public void Wachse()
    {
        if (SpielerController.Zaubert == false)
            return;

        var zuFaerbendePixel = ((float)_zielPixel.Length) * Prozent;
        var differenz = zuFaerbendePixel - _gefaerbtePixel.Count;

        for (int i = 0; i < differenz - 1; i++)
        {
            var positon = GetTransparenterPixel();
            if (positon < 0)
                break;

            _aktuellePixel[positon] = _zielPixel[positon];

            _transparentePixel.Remove(positon);
            _gefaerbtePixel.Add(positon);
        }

        _aktuelleTexture.SetPixels(_aktuellePixel);
        _aktuelleTexture.Apply();

        Prozent += SpielerController.ZauberKraftProzent;

        if (Prozent > 1f)
            Prozent = 1f;

        if (Prozent >= 1f && _naechstePhase != null && UnityEngine.Random.Range(0, SpielerController.PflanzungsPhaseUebergangsWert) <= 1)
        {
            AudioController.Instanz.PlaySound(AudioHelper.Instanz.plantSpawnEffect);
            LevelUpKontroller.Instanz.ExpErhalten(1);
            OnPhaseWachsenEndeHandler(_naechstePhase);
            _aktuelleTexture.SetPixels(_zielPixel);
            _aktuelleTexture.Apply();
        }
    }

    private int GetTransparenterPixel()
    {
        if (_transparentePixel.Count <= 0)
            return -1;

        return _transparentePixel[UnityEngine.Random.Range(0, _transparentePixel.Count)];
    }

    private int GetGefaerbterPixel()
    {
        if (_gefaerbtePixel.Count <= 0)
            return -1;

        return _gefaerbtePixel[UnityEngine.Random.Range(0, _gefaerbtePixel.Count)];
    }
}

