﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpriteHelper : MonoBehaviour
{
    void Start()
    {
        if (_renderer == null)
            _renderer = GetComponent<SpriteRenderer>();

        _renderer.sprite = _sprites[Random.Range(0, _sprites.Length)];
    }

    [SerializeField]
    private SpriteRenderer _renderer = null;

    [SerializeField]
    private Sprite[] _sprites = null;
}
