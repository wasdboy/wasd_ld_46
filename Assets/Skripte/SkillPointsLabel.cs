﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SkillPointsLabel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LevelUpKontroller.Instanz.SkillPunkteChanged += OnSkillPunkteChanged;
        OnSkillPunkteChanged();
    }

    [SerializeField]
    private TextMeshProUGUI _expLabel;

    private void OnEnable()
    {
        OnSkillPunkteChanged();
    }

    public void OnSkillPunkteChanged()
    {
        _expLabel.text = string.Format("Skillpoints: {0}", LevelUpKontroller.Instanz.Skillpunkte.ToString());
    }
}
