﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillUi : MonoBehaviour
{
    private void Start()
    {
        if(_image ==  null)
        _image = GetComponent<Image>();

        _defaultFarbe = _image.color;
    }

    [SerializeField]
    private Image _image;
    private Vector3 _hoverSize = new Vector3(1.1f, 1.1f, 1);
    private Vector3 _normalSize = new Vector3(1f, 1f, 1);
    private Color _defaultFarbe;
    public Color GekauftFarbe;
    public EventTrigger Trigger;

    public void SkillKaufen()
    {
        _image.color = GekauftFarbe;
        Trigger.enabled = false;

        if (name.Contains("Infinity"))
            StartCoroutine(Reactivieren());
    }

    private IEnumerator Reactivieren()
    {
        yield return new WaitForSeconds(0.5f);
        Trigger.enabled = true;
        _image.color = _defaultFarbe;
    }

    public void HoverStart()
    {
        transform.parent.localScale = _hoverSize;
    }

    public void HoverEnd()
    {
        transform.parent.localScale = _normalSize;
    }
}
