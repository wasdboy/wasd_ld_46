﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpielerController : MonoBehaviour
{
    private void Start()
    {
        _circleCollider2D = GetComponent<CircleCollider2D>();
    }

    [SerializeField]
    private Rigidbody2D _rigidbody2D = null;

    [SerializeField]
    private Animator _animator = null;

    [SerializeField]
    private ParticleSystem _partikelSystem = null;

    [SerializeField]
    private static CircleCollider2D _circleCollider2D = null;

    public static float Geschwindigkeit = 4f;
    public static float ZauberBewegungsMalus = 0.4f;

    public static int PflanzungsPhaseUebergangsWert = 10000;
    public static float ObjektSpawnPhaseMalus = 0.2f;
    public static bool KannTiereSpawnen;
    public static float ZauberKraftProzent = 0.0125f;
    public static bool Zaubert;

    public static float ZauberRadius { get { return _circleCollider2D.radius; } set { _circleCollider2D.radius = value; } }

    private float _animalSpawnTimer;

    public GameObject SquirrelPrefab;
    public GameObject ButterflyPrefab;
    public GameObject BumbleBeePrefab;

    private void OnTriggerStay2D(Collider2D collision)
    {
        var tile = collision.GetComponent<TileWachsenEffekt>();
        if (tile == null)
            return;

        if (Zaubert)
            tile.StarteWachsen();
        else
            tile.BeendeWachsen();
    }


    public void Cheat()
    {
        Debug.Log("CHEAT");

        KannTiereSpawnen = true;
        Geschwindigkeit = 7f;
        ZauberRadius = 3f;
        PflanzungsPhaseUebergangsWert = 1000;
        ObjektSpawnPhaseMalus = 1f;
        ZauberKraftProzent = 0.3f;

        ObjektSpawnKontroller.Instanz.KannBaeumeSpawnen = true;
        ObjektSpawnKontroller.Instanz.KannBlumenSpawnen = true;
        ObjektSpawnKontroller.Instanz.KannPilzeSpawnen = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var tile = collision.GetComponent<TileWachsenEffekt>();
        if (tile == null)
            return;

        tile.BeendeWachsen();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Zaubert = true;
            AudioController.Instanz.SetCastEffectStatus(true);            
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            AudioController.Instanz.SetCastEffectStatus(false);
            Zaubert = false;
        }


        var geschwindigkeit = Geschwindigkeit;

        if (Zaubert)
        {
            geschwindigkeit = Geschwindigkeit * ZauberBewegungsMalus;
            SpawnAnimal();
        }
        if(_partikelSystem.isPlaying == false && Zaubert)
            _partikelSystem.Play();
        else if(_partikelSystem.isPlaying && Zaubert == false)
            _partikelSystem.Stop();

        _rigidbody2D.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * geschwindigkeit;
        _animator.SetFloat("Laufen", Mathf.Abs(_rigidbody2D.velocity.x) + Mathf.Abs(_rigidbody2D.velocity.y));

        if (_rigidbody2D.velocity.x < 0 && transform.rotation.y != 180)
            transform.rotation = Quaternion.Euler(0, 180, 0);

        if (_rigidbody2D.velocity.x > 0 && transform.rotation.y != 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    private void SpawnAnimal()
    {
        if (KannTiereSpawnen == false)
            return;

        if (_animalSpawnTimer > 0)
        {
            _animalSpawnTimer -= Time.deltaTime;
            return;
        }

        var random = Random.Range(1, 10);
        GameObject prefab = null;
        Vector3 position = Vector3.zero;

        if (random >= 9 && EntityController.Instanz.Trees.Count > 0)
        {
            prefab = SquirrelPrefab;
            position = EntityController.Instanz.GetRandomTree().transform.position;
        }
        else if(random >= 5 && EntityController.Instanz.Flowers.Count > 0)
        {
            prefab = ButterflyPrefab;
            position = EntityController.Instanz.GetRandomFlower().transform.position;
        }
        else if (EntityController.Instanz.Flowers.Count > 0)
        {
            prefab = BumbleBeePrefab;
            position = EntityController.Instanz.GetRandomFlower().transform.position;
        }

        if (prefab == null || position == Vector3.zero)
            return;

        LevelUpKontroller.Instanz.ExpErhalten(1);
        Instantiate(prefab, position, Quaternion.identity);
        

        _animalSpawnTimer = Random.Range(3, 10);
    }
}
