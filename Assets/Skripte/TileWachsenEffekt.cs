﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileWachsenEffekt : MonoBehaviour
{
    private void Start()
    {
        Phase = new PflanzungsPhase(WiesenSprite, WiesenRenderer, BodenRenderer);
        Phase.OnPhaseWachsenEndeHandler += PhaseWachsenEnded;
        Phase.Positon = transform.position;
    }

    private bool _istAmWachsen;

    public IPhase Phase;

    public Sprite WiesenSprite;
    public SpriteRenderer WiesenRenderer;
    public SpriteRenderer BodenRenderer;

    public void FixedUpdate()
    {
        SpriteUpdaten();
    }

    public void StarteWachsen()
    {
        _istAmWachsen = true;
    }

    public void BeendeWachsen()
    {
        _istAmWachsen = false;
    }

    public void SpriteUpdaten()
    {
        if (_istAmWachsen)
            Wachse();
        else
            Verrotte();
    }

    private void Wachse()
    {
        Phase.Wachse();
    }

    private void Verrotte()
    {
        Phase.Verrotte();
    }

    private void PhaseWachsenEnded(IPhase neuePhase)
    {
        Phase.OnPhaseWachsenEndeHandler -= PhaseWachsenEnded;

        var altePhase = Phase;
        Phase = neuePhase;
        Phase.Positon = transform.position;

        Phase.OnPhaseWachsenEndeHandler += PhaseWachsenEnded;
    }
}
