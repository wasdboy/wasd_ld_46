﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follow : MonoBehaviour
{
    [SerializeField]
    private Transform _ziel = null;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(_ziel.position.x, _ziel.position.y, transform.position.z);
    }
}
