﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundUi : MonoBehaviour
{
    void Start()
    {
        _musicSlider.onValueChanged.AddListener(OnMusicVolumeChanged);
        _effectSlider.onValueChanged.AddListener(OnEffectVolumeChanged);

        DontDestroyOnLoad(gameObject);

        _musicSlider.value = 0.5f;
        _effectSlider.value = 0.5f;
    }

    [SerializeField]
    public Slider _musicSlider, _effectSlider;

    private void OnMusicVolumeChanged(float value)
    {
        AudioController.Instanz.SetMusicVolume(value);
    }

    private void OnEffectVolumeChanged(float value)
    {
        AudioController.Instanz.SetEffectVolume(value);
    }
}
