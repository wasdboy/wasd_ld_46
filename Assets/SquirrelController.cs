﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquirrelController : MonoBehaviour, IAnimal
{
    void Start()
    {
        _idleZeit = Random.Range(15, 120);
    }

    private SpriteRenderer _renderer;

    [SerializeField]
    private Animator _animator;

    public Entity _ziel;
    private bool _idle;

    [SerializeField]
    private bool _randomFarbe;

    private float _idleZeit;
    private float _verstecktZeit;

    private void Update()
    {
        if (_ziel == null)
        {
            _ziel = EntityController.Instanz.GetRandomTree();

            if (_ziel == null)
                return;

            _ziel.Animals.Add(this);

            var offset = new Vector2(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f));
            transform.position = new Vector3(_ziel.transform.position.x + offset.x, _ziel.transform.position.y + offset.y);
        }

        if (_idle)
            Idle();
        else
            Verstecken();
    }

    private void Verstecken()
    {
        if (_verstecktZeit > 0)
        {
            _verstecktZeit -= Time.deltaTime;
            return;
        }

        _idle = true;
        _idleZeit = Random.Range(15, 120);
        _animator.SetTrigger("Emerge");
              
    }

    private void Idle()
    {
        if (_idleZeit > 0)
        {
            _idleZeit -= Time.deltaTime;
            return;
        }

        _ziel = null;
        _idle = false;
        _animator.SetTrigger("Hide");
    }

    public void NeueZielSuchen()
    {
        _idle = false;
        _animator.SetTrigger("Hide");
        _verstecktZeit = 5f;
    }
}
